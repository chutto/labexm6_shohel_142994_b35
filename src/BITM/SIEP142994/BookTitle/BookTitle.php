<?php
namespace App\BookTitle;

use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
use PDO;
class BookTitle extends DB{
    public $id="";
    public $book_title="";
    public $author_name="";

    public function __construct()
    {
        parent::__construct();
    }

    public function setData($data=Null)
    {

        if(array_key_exists('id',$data))
        {

            $this->id=$data['id'];
        }

        if(array_key_exists('book_title',$data))
        {
            $this->book_title=$data['book_title'];


        }

        if(array_key_exists('author_name',$data))
        {

            $this->author_name=$data['author_name'];

        }

    }



    public function store()

    {


        $arrData = array($this->book_title,$this->author_name);
        $sql = "INSERT INTO book_title(book_title,author_name) VALUES (?,?)";

        $STH = $this->DBH->prepare($sql);
        $result=$STH->execute($arrData);

        if($result)
            Message::message("Success!!!!,Data Has Been Inserted Successfully:)");
        else
            Message::message("Failed!!!!,Data Has not Been Inserted Successfully");
        Utility::redirect('create.php');


    }



    public function index()
    {
        echo "I am inside index method";
    }
}